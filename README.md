# PukaPuka - my first game 😄 

Ship vs a deadly skull. Survive as long as you can! 

## Requirements:

- Python 3.x (I used 3.8)

## Controls:

- Move: Arrow keys
- Restart: Tab
- Pause: Esc

## Screenshots:

Main battle:

![](demos/demo1.png)

BOSS Special #1 - laser gun

![](demos/demo2.png)

BOSS Special #2 - quadruple pyramid shot

![](demos/demo3.png)

BOSS Special #3 - Close combat SOLO

![](demos/demo4.png)
